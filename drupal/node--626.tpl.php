<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<section class="introduction">
  <?php print($lp_blocks[0]); ?>
</section>

<section class="advantages">
  <?php print($lp_blocks[1]); ?>
  <?php print($lp_blocks[2]); ?>
</section>

<section class="sliders">
  <?php print($lp_blocks[3]); ?>
</section>

<section class="minor-info">
  <?php print($lp_blocks[4]); ?>
</section>

<section class="chars">
  <?php print($lp_blocks[5]); ?>
</section>

<section class="promotion">
  <?php print($lp_blocks[6]); ?>
</section>

<section class="minor-info minor-info--advantages">
  <?php print($lp_blocks[7]); ?>
</section>

<footer class="footer">
  <?php print($lp_blocks[8]); ?>
  <?php print($lp_blocks[9]); ?>
</footer>


</article><!-- /.node -->

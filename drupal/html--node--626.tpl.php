<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" <?php print $html_attributes; ?>><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" <?php print $html_attributes; ?>><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" <?php print $html_attributes; ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9" <?php print $html_attributes; ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html <?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->

<head profile="<?php print $grddl_profile; ?>">
  <title><?php print $head_title; ?></title>
  <link rel="preload" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;700;800;900&display=swap" as="style">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;700;800;900&display=swap">
  <link rel="preconnect" href="https://www.google.com">
  <?php print $head; ?>
  <?php print $styles; ?>
  <meta name="MobileOptimized" content="width">
  <meta name="HandheldFriendly" content="true">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="cleartype" content="on">
  <link rel="stylesheet" href="/sites/all/themes/mytheme/lp/build/css/normalize.css">
  <link rel="stylesheet" href="/sites/all/themes/mytheme/lp/build/css/vendor.css">
  <link rel="stylesheet" href="/sites/all/themes/mytheme/lp/build/css/custom.css">
</head>


<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $scripts; ?>
  <script src="/sites/all/themes/mytheme/lp/build/js/vendor.js"></script>
  <script src="/sites/all/themes/mytheme/lp/build/js/custom.js"></script>
  <?php print $page_bottom; ?>
</body>


</html>

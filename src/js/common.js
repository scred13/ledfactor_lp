(function ($, Drupal) {
  Drupal.behaviors.asd2eq = {
    attach: function (context) {
      if (typeof (initialized) == 'undefined') {
        initialized = true;
        var swiper = new Swiper('.advantages__content .swiper-container', {
          autoplay: true,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
        });

        var swiper = new Swiper('.sliders .sliders__item--first .swiper-container', {
          watchOverflow: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });

        var swiper = new Swiper('.sliders .sliders__item--second .swiper-container', {
          watchOverflow: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });

        // var swiper = new Swiper('.promotion .swiper-container', {
        //   watchOverflow: true,
        //   slidesPerView: 'auto',
        //   freeMode: true,
        // });


        $('.header__phones-toggler').click(function () {
          $(this).siblings('.header__phones-items').toggleClass('js--active');
        })

        $(document).on('click',function (e) {
          const el = '.header__phones-toggler';
          if ($(e.target).closest(el).length) return;
          $('.header__phones-items').removeClass('js--active');
        });

        // const ps = new PerfectScrollbar('.promotion .promotion__content .content-right', {
        //   wheelSpeed: 1,
        //   wheelPropagation: false,
        //   minScrollbarLength: 20,
        //   suppressScrollY: true,
        // });

      }


      //Код написанный здесь будет выполнен при каждом AJAX запросе

    }
  };
}(jQuery, Drupal));




var SITE_URL = 'http://bondarenko.industrialmedia.com.ua';

var gulp                    = require('gulp'),
    gutil                   = require('gulp-util'),
    sourcemaps              = require('gulp-sourcemaps'),
    sass                    = require('gulp-sass'),
    browserSync             = require('browser-sync'),
    concat                  = require('gulp-concat'),
    uglify                  = require('gulp-uglify'),
    cleanCSS                = require('gulp-clean-css'),
    rename                  = require('gulp-rename'),
    del                     = require('del'),
    plumber                 = require('gulp-plumber'),
    autoprefixer            = require('gulp-autoprefixer'),
    spritesmith             = require('gulp.spritesmith'),
    ftp                     = require('vinyl-ftp'),
    notify                  = require("gulp-notify"),
    rigger                  = require('gulp-rigger'),
    debug                   = require('gulp-debug'),
    svgSprite               = require('gulp-svg-sprite'),
    gulpif                  = require('gulp-if'),
    exit                    = require('gulp-exit'),
    fileinclude             = require('gulp-file-include'),
    arg                     = require('yargs').argv;


// Сторонние cкрипты проекта
gulp.task('scripts:vendor', function () {
    return gulp.src([
    'src/js/vendor/*.js',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('build/js'))
        .pipe(browserSync.reload({stream: true}));
});

// Кастомный скрипт проекта
gulp.task('scripts:custom', function () {
    return gulp.src([
    'src/js/common.js',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('custom.js'))
        .pipe(gulp.dest('build/js'))
        .pipe(browserSync.reload({stream: true}));
});

// Сторонние стили проекта
gulp.task('styles:vendor', function () {
    return gulp.src([
    'src/style/vendor/*.css',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({stream: true}));
});

// normalize
gulp.task('styles:normalize', function () {
    return gulp.src([
    'src/style/normalize.css',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('normalize.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({stream: true}));
});

// Кастомные стили проекта
gulp.task('styles:sass', function () {
    return gulp.src('src/style/custom.sass')
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", notify.onError()))
        .pipe(rename({
            suffix: '',
            prefix: ''
        }))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({stream: true}));
});

// Browser sync
// gulp.task('browser:sync', function() {
//   browserSync.init({
//     proxy: SITE_URL,
//     ws: true,
//     serveStatic: [{
//       route: '/themes/mytheme/build',
//       dir: 'build'
//     }],
//     open: true
//   });
// });

gulp.task('browser:sync', function () {
  browserSync({
    server: {
      baseDir: 'build'
    },
    notify: false,
    // tunnel: true,
    // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
  });
});


// gulp.task('html', function () {
//   return gulp.src('src/template/**/*.html') //Выберем файлы по нужному пути
//     .pipe(rigger()) //Прогоним через rigger
//     .pipe(gulp.dest('build')) //Выплюнем их в папку build
//     .pipe(browserSync.stream());
// });

gulp.task('html', function() {
  return gulp.src('src/template/**/*.html')
    .pipe(fileinclude({
      prefix: '@@',
    }))
    .pipe(gulp.dest('build'))
    .pipe(browserSync.stream());
});

// Watch
gulp.task('watch', function () {
    gulp.watch('src/style/**/*.sass', gulp.parallel('styles:sass'));
    gulp.watch(['src/*.html', 'src/template/**/*.html'], gulp.series('html'));
    gulp.watch(['js/**/*.js', 'src/js/common.js'], gulp.parallel('scripts:vendor', 'scripts:custom'));
    gulp.watch('src/static/**/*', gulp.parallel('files:static'));
    gulp.watch('src/img/**/*.{png,svg,jpg,gif}', gulp.series('sprite:svg', 'images'));
});

// Images
gulp.task('images', function () {
    return gulp.src("src/img/**/*")
        .pipe(plumber())
        .pipe(gulp.dest('build/img'));
});

// Svg sprites
gulp.task('sprite:svg', function () {
    return gulp.src('src/img/sprite/**/*.svg')
        .pipe(svgSprite({
            mode: {
                css: {
                    dest  : '.',
                    bust  : true,
                    sprite: '../img/sprite-svg.svg',
                    layout: 'vertical',
                    prefix: '',
                    dimensions: true,
                    render: {
                        scss: {
                            dest: 'sprite-svg'
                        }
                    }
                }
            },
            shape: {
                spacing: {
                    padding: 1
                }
            }
        }))
        .pipe(debug({title: 'style:svg'}))
        .pipe( gulpif('*.scss', gulp.dest('src/sass/tmp'), gulp.dest('build/img')));
});

// Png sprites
gulp.task('sprite:png', function(done) {
    var spriteData = gulp.src('src/img/sprite/**/*.png')
            .pipe(spritesmith({
                imgName: '../img/sprite-png.png',
                cssName: 'sprite-png.sass'
            }));

      spriteData.img.pipe(gulp.dest('build/img'));
      spriteData.css.pipe(gulp.dest('src/sass/tmp/'));
    done();
});

gulp.task('files:static', function () {
  return gulp.src([
    'src/static/**/*',
  ])
    .pipe(gulp.dest('build/static'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('build:scripts', gulp.series('scripts:vendor', 'scripts:custom'));
gulp.task('build:styles', gulp.series('styles:sass', 'styles:normalize', 'styles:vendor'));
gulp.watch('src/img/**/*.{png,svg}', gulp.series('images', 'sprite:svg' , 'sprite:png'));

gulp.task('removedist', function () {
    return del('build');
});

gulp.task('build', gulp.series('removedist', gulp.parallel('build:scripts', 'build:styles', 'images', 'sprite:svg', 'sprite:png', 'files:static')));

gulp.task('deploy:upload', function(done) {
  var conn = ftp.create( {
      host:     arg.host,
      user:     arg.user,
      password: arg.pass,
      parallel: 10,
  } );


gulp.src( ['**/*.*', '!node_modules/**', '!gulpfile.js', '!package.json', '!package-lock.json', '!src/**'], { base: '.', buffer: false} )
      .pipe( conn.newer( '/' ) )
      .pipe(debug({title: 'deploy:upload'}))
      .pipe( conn.dest( '/themes/mytheme/' ) )
      .pipe(exit());
 done();
});

gulp.task('deploy', gulp.series('build', 'deploy:upload'));

gulp.task('default', gulp.parallel('watch', 'html', 'images', 'styles:sass', 'styles:normalize', 'styles:vendor', 'scripts:vendor', 'scripts:custom', 'sprite:svg', 'sprite:png', 'styles:vendor', 'files:static', 'browser:sync'));
